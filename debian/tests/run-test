#!/bin/bash

set -euxo pipefail

echo "Downloading repository of regression files..."
mkdir -p data
pushd data
git -c http.sslVerify=false clone -n --depth=1 --filter=tree:0 https://github.com/cclib/cclib.git
pushd cclib
git sparse-checkout set --no-cone data
git -c http.sslVerify=false checkout
mv data/* ..
popd
git -c http.sslVerify=false clone --depth=1 --filter=tree:0 https://github.com/cclib/cclib-data.git regression
popd

echo "Running tests..."
python3 -m pytest -v --capture=no --terse test -k "not (test_method or bridge or io or parser or psi4 or pyscf or pyquante)"
python3 -m pytest -v --capture=no -k test_regression test/regression.py
python3 -m pytest -v -s test/regression_io.py

python3 -m test.test_utils
python3 -m test.test_data --terse
python3 -m test.regression --traceback
python3 -m test.regression_io
